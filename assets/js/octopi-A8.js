	var urlJobA8 = "http://OCTOPRINT-IP/api/job?apikey=OCTOPRINT-API-KEY";
    var urlPrinterA8 = "http://OCTOPRINT-IP/api/printer?apikey=OCTOPRINT-API-KEY";
    
	(function($) {
	  $(document).ready(function() {
	      $.ajaxSetup({cache: false});
///A8
	       setInterval(function () {
			
				$.getJSON(urlJobA8, function(jsonA8) {
					   //A8 calc time2go
							unixTimestamp = jsonA8.progress.printTimeLeft; 
							
							// convert to milliseconds and  
							// then create a new Date object 
							dateObj = new Date(unixTimestamp * 1000); 
							utcString = dateObj.toUTCString(); 
							
							time2goA8 = utcString.slice(-11, -4);
						
						//A8 calc time4now
							unixTimestamp = jsonA8.progress.printTime; 
							
							// convert to milliseconds and  
							// then create a new Date object 
							dateObj = new Date(unixTimestamp * 1000); 
							utcString = dateObj.toUTCString(); 
							
							time4nowA8 = utcString.slice(-11, -4); 
						
						//A8 progress pecent
							if (jsonA8.progress.completion != null) {
								var num1 = jsonA8.progress.completion;
								var A8percent = num1.toFixed(2);
							} else {
								var A8percent = '--';
							}
							
					//populate html  
						var A8State = (jsonA8.state);
						if (A8State == 'Offline') {$('#A8.printerBox').addClass('offline')}else{$('#A8.printerBox').removeClass('offline')}
					
					
// 						$('#A8 .state').text(jsonA8.state);
						$('#A8 .fileName').text(jsonA8.job.file.name);
						$('#A8 .percent').text(A8percent+ ' %');
						$('#A8 .timeETA').text(time2goA8);   
						$('#A8 .timePrint').text(time4nowA8);            
				  
			});
		
				$.getJSON(urlPrinterA8, function(jsonA8printer) {
					//populate html					
						$('#A8 .bedTemp').text(jsonA8printer.temperature.bed.actual);
							$('#A8 .bedTempT').text(jsonA8printer.temperature.bed.target);
						
						$('#A8 .end0Temp').text(jsonA8printer.temperature.tool0.actual);
							$('#A8 .end0TempT').text(jsonA8printer.temperature.tool0.target);
				
					//Printer status msgs
						var A8flagsCancelling = (jsonA8printer.state.flags.cancelling);
							//if (A8flagsCancelling == true) {$('#A8 .extraInfo.canceling').html('<li>Cancelling: <b>true</b></li>')} else {$('#A8 .extraInfo').html('<li>Cancelling: <b>false</b></li>')}
							if (A8flagsCancelling == true) {$('#A8.printerBox').addClass('canceling')}else{$('#A8.printerBox').removeClass('canceling')}
							
						var A8flagsclosedOrError = (jsonA8printer.state.flags.closedOrError);
							//if (A8flagsclosedOrError == true) {$('#A8 .extraInfo.closedOrError').html('closedOrError: true<br />')} else {$('#A8 .extraInfo').html('closedOrError: <b>false</b> <br />')}	
							if (A8flagsclosedOrError == true) {$('#A8.printerBox').addClass('colsedOrError')}else{$('#A8.printerBox').removeClass('colsedOrError')}
							
						var A8flagserror = (jsonA8printer.state.flags.error);
							//if (A8flagserror == true) {$('#A8 .extraInfo.error').html('error: true<br />')} else {$('#A8 .extraInfo').html('error: <b>false</b> <br />')}	
							if (A8flagserror == true) {$('#A8.printerBox').addClass('error')}else{$('#A8.printerBox').removeClass('error')}
							
						var A8flagsfinishing = (jsonA8printer.state.flags.finishing);
							//if (A8flagsfinishing == true) {$('#A8 .extraInfo.finishing').html('finishing: true<br />')} else {$('#A8 .extraInfo').html('finishing: <b>false</b> <br />')}
							if (A8flagsfinishing == true) {$('#A8.printerBox').addClass('finishing')}else{$('#A8.printerBox').removeClass('finishing')}

							
						var A8flagsOperational = (jsonA8printer.state.flags.operational);
							//if (A8flagsOperational == true) {$('#A8 .extraInfo.operational').append('operational: true<br />')} else {$('#A8 .extraInfo').append('operational: <b>false</b> <br />')}
							if (A8flagsOperational == true) {$('#A8.printerBox').addClass('operational')}else{$('#A8.printerBox').removeClass('operational')}
							
						var A8flagspaused = (jsonA8printer.state.flags.paused);
							//if (A8flagspaused == true) {$('#A8 .extraInfo.paused').html('paused: true<br />')} else {$('#A8 .extraInfo').html('paused: <b>false</b> <br />')}	
							if (A8flagspaused == true) {$('#A8.printerBox').addClass('paused')}else{$('#A8.printerBox').removeClass('paused')}
							
						var A8flagspausing = (jsonA8printer.state.flags.pausing);
							//if (A8flagspausing == true) {$('#A8 .extraInfo').html('pausing: true<br />')} else {$('#A8 .extraInfo').html('pausing: <b>false</b> <br />')}	
							if (A8flagspausing == true) {$('#A8.printerBox.pausing').addClass('pausing')}
							
						var A8flagsprinting = (jsonA8printer.state.flags.printing);
							//if (A8flagsprinting == true) {$('#A8 .extraInfo').html('printing: true<br />')} else {$('#A8 .extraInfo').html('printing: <b>false</b> <br />')}					
							if (A8flagsprinting == true) {$('#A8.printerBox.printing').addClass('printing')}
							
						var A8flagsready = (jsonA8printer.state.flags.ready);
							//if (A8flagsready == true) {$('#A8 .extraInfo.ready').append('ready: true<br />')} else {$('#A8 .extraInfo').append('ready: <b>false</b> <br />')}					
							if (A8flagsready == true) {$('#A8.printerBox').addClass('ready')}
							
						var A8flagsresuming = (jsonA8printer.state.flags.resuming);
							//if (A8flagsresuming == true) {$('#A8 .extraInfo.resuming').html('resuming: true<br />')} else {$('#A8 .extraInfo').html('resuming: <b>false</b> <br />')}					
							if (A8flagsresuming == true) {$('#A8.printerBox').addClass('resuming')}
							
						var A8flagssdReady = (jsonA8printer.state.flags.sdReady);
							//if (A8flagssdReady == true) {$('#A8 .extraInfo.sd').html('sdReady: true<br />')} else {$('#A8 .extraInfo').html('sdReady: <b>false</b> <br />')}	
							if (A8flagssdReady == true) {$('#A8.printerBox .fa.sdReady').removeClass('hideMe')}else{$('#A8.printerBox .fa.sdReady').addClass('hideMe')}
			
				});



	       }, 1000);
	  	});
	})(jQuery);     
